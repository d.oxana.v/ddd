<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();
    
    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        $messages[] = 'Thank you, form was saved';
    }
    
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['data'] = !empty($_COOKIE['data_error']);
    $errors['pol'] = !empty($_COOKIE['pol_error']);
    $errors['konch'] = !empty($_COOKIE['konch_error']);
    $errors['sverh'] = !empty($_COOKIE['sverh_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    
    if ($errors['fio']) {
        setcookie('fio_error', '', 100000);
        $messages[] = '<div class="error">Write name</div>';
    }
    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div class="error">Write email</div>';
    }
    if ($errors['data']) {
        setcookie('data_error', '', 100000);
        $messages[] = '<div class="error">Write date</div>';
    }
    if ($errors['pol']) {
        setcookie('pol_error', '', 100000);
        $messages[] = '<div class="error">Write gender</div>';
    }
    if ($errors['konch']) {
        setcookie('konch_error', '', 100000);
        $messages[] = '<div class="error">Write limbs</div>';
    }
    if ($errors['sverh']) {
        setcookie('sverh_error', '', 100000);
        $messages[] = '<div class="error">Write abilities</div>';
    }
    if ($errors['bio']) {
        setcookie('bio_error', '', 100000);
        $messages[] = '<div class="error">Write biography</div>';
    }
    
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['data'] = empty($_COOKIE['data_value']) ? '' : $_COOKIE['data_value'];
    $values['pol'] = empty($_COOKIE['pol_value']) ? '' : $_COOKIE['pol_value'];
    $values['konch'] = empty($_COOKIE['konch_value']) ? '' : $_COOKIE['konch_value'];
    $values['sverh'] = empty($_COOKIE['sverh_value']) ? '' : $_COOKIE['sverh_value'];
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
    include('index.php');
}
else {
    $errors = FALSE;
    if (empty($_POST['Name'])) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('fio_value', $_POST['Name'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['E_mail'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('email_value', $_POST['E_mail'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['Date'])) {
        setcookie('data_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('data_value', $_POST['Date'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['Gender'])) {
        setcookie('pol_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('pol_value', $_POST['Gender'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['Number_of_limbs'])) {
        setcookie('konch_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('konch_value', $_POST['Number_of_limbs'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['Superpowers'])) {
        setcookie('sverh_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('sverh_value', $_POST['Superpowers'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['Biografia'])) {
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('bio_value', $_POST['Biografia'], time() + 30 * 24 * 60 * 60);
    }
    
    
    if ($errors) {
        header('Location: form.php');
        exit();
    }
    else {
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('data_error', '', 100000);
        setcookie('pol_error', '', 100000);
        setcookie('konch_error', '', 100000);
        setcookie('sverh_error', '', 100000);
        setcookie('bio_error', '', 100000);
    }
    
    setcookie('save', '1');         
   
   
       $connection = 'mysql:host=localhost;dbname=u17310';
       $pdo = new PDO($connection, 'u17310', '2610050');
       $sql = 'INSERT INTO lol SET Name = ?, E_mail = ?, Date = ?, Gender = ?, Number_of_limbs = ?,
Superpowers = ?, Biografia = ?';
       $stmt = $pdo->prepare($sql);
       
       $stmt->execute(array($_POST['Name'], $_POST['E_mail'], $_POST['Date'], $_POST['Gender'],
           $_POST['Number_of_limbs'], serialize($_POST['Superpowers']), $_POST['Biografia']));
       
       header('Location: form.php');
}